local m=120155035
local cm=_G["c"..m]
cm.name="芳香岩浆熊"
function cm.initial_effect(c)
	--Recover
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(m,0))
	e1:SetCategory(CATEGORY_RECOVER)
	e1:SetType(EFFECT_TYPE_IGNITION)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCost(cm.cost)
	e1:SetTarget(cm.target)
	e1:SetOperation(cm.operation)
	c:RegisterEffect(e1)
end
--Recover
cm.cost=RD.CostSendHandToGrave(Card.IsAbleToGraveAsCost,1,2,nil,nil,Group.GetCount)
function cm.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	RD.TargetRecover(tp,e:GetLabel()*300)
end
function cm.operation(e,tp,eg,ep,ev,re,r,rp)
	RD.Recover(nil,e:GetLabel()*300)
end