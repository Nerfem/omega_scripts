local m=120244056
local cm=_G["c"..m]
cm.name="帝王境界"
function cm.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCost(cm.cost)
	c:RegisterEffect(e1)
	--Atk Up
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_UPDATE_ATTACK)
	e2:SetRange(LOCATION_FZONE)
	e2:SetTargetRange(LOCATION_MZONE,LOCATION_MZONE)
	e2:SetTarget(cm.uptg)
	e2:SetValue(400)
	c:RegisterEffect(e2)
end
--Activate
cm.cost=RD.CostSendDeckTopToGrave(2)
--Atk Up
function cm.uptg(e,c)
	return c:IsFaceup() and c:GetBaseDefense()==1000 and c:IsLevelAbove(5)
end