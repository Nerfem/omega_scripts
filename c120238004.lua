local m=120238004
local cm=_G["c"..m]
cm.name="合成魔兽 加泽特"
function cm.initial_effect(c)
	--Atk Up
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_MATERIAL_CHECK)
	e1:SetValue(cm.check)
	c:RegisterEffect(e1)
	--Summon Only
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_SUMMON_COST)
	e2:SetOperation(cm.facechk)
	e2:SetLabelObject(e1)
	c:RegisterEffect(e2)
end
--Atk Up
function cm.check(e,c)
	local atk=0
	local g=c:GetMaterial()
	if g:GetCount()==1 then
		atk=RD.GetBaseAttackOnTribute(g:GetFirst())*2
	elseif g:GetCount()==2 then
		atk=RD.GetBaseAttackOnTribute(g:GetFirst())+RD.GetBaseAttackOnTribute(g:GetNext())
	end
	if atk>0 and e:GetLabel()==1 then
		e:SetLabel(0)
		RD.AttachAtkDef(e,c,atk,0,RESET_EVENT+0xff0000)
	end
end
--Summon Only
function cm.facechk(e,tp,eg,ep,ev,re,r,rp)
	e:GetLabelObject():SetLabel(1)
end