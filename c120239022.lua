local m=120239022
local list={120239024,120239026}
local cm=_G["c"..m]
cm.name="超魔犬 舔舔机器犬"
function cm.initial_effect(c)
	RD.AddCodeList(c,list)
	--To Hand
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(m,0))
	e1:SetCategory(CATEGORY_TOHAND+CATEGORY_GRAVE_ACTION)
	e1:SetType(EFFECT_TYPE_IGNITION)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCost(cm.cost)
	e1:SetOperation(cm.operation)
	c:RegisterEffect(e1)
end
--To Hand
function cm.thfilter(c)
	return ((c:IsLevel(10) and c:IsType(TYPE_MAXIMUM)) or c:IsCode(list[1]) or c:IsCode(list[2]))
		and c:IsAbleToHand()
end
cm.cost1=RD.CostPayLP(500)
cm.cost2=RD.CostSendHandToDeck(Card.IsAbleToDeckAsCost,2,2,false)
function cm.cost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return cm.cost1(e,tp,eg,ep,ev,re,r,rp,chk) and cm.cost2(e,tp,eg,ep,ev,re,r,rp,chk) end
	cm.cost1(e,tp,eg,ep,ev,re,r,rp,chk)
	cm.cost2(e,tp,eg,ep,ev,re,r,rp,chk)
end
function cm.operation(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if c:IsFaceup() and c:IsRelateToEffect(e) then
		RD.AttachBattleIndes(e,c,1,aux.Stringid(m,2),RESET_EVENT+RESETS_STANDARD+RESET_DISABLE+RESET_PHASE+PHASE_END+RESET_OPPO_TURN)
		RD.CanSelectAndDoAction(aux.Stringid(m,1),HINTMSG_ATOHAND,aux.NecroValleyFilter(cm.thfilter),tp,LOCATION_GRAVE,0,1,1,nil,function(g)
			RD.SendToHandAndExists(g,1-tp)
		end)
	end
end