local m=120235039
local list={120235024,120235021,120196050}
local cm=_G["c"..m]
cm.name="苍救之泡影 阿卢蒂埃拉"
function cm.initial_effect(c)
	RD.AddCodeList(c,list)
	--Fusion Material
	RD.AddFusionProcedure(c,list[1],list[2])
	--Cannot Activate
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_CANNOT_ACTIVATE)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetRange(LOCATION_MZONE)
	e1:SetTargetRange(0,1)
	e1:SetValue(cm.limit)
	c:RegisterEffect(e1)
end
--Cannot Activate
function cm.limit(e,re,tp)
	local tc=re:GetHandler()
	return not tc:IsCode(list[3]) and tc:GetType()==TYPE_SPELL
		and re:IsHasType(EFFECT_TYPE_ACTIVATE) and re:IsActiveType(TYPE_SPELL)
end